#!/usr/bin/env python

# Copyright (c) 2014, Ole Lund, Technical University of Denmark
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Import libraries
#
import sys, time
import os
import gc
import numpy as np
import array
from optparse import OptionParser
from operator import itemgetter
import re
#import gzip
#
# Functions
#
# Start time to keep track of progress
#
#t0 = time.time()
#
#
#
#etta = 0.001
#
# Parse command line options
#
parser = OptionParser()
parser.add_option("-i", "--inputfile", dest="inputfilename", help="read from INFILE", metavar="INFILE")
parser.add_option("-o", "--outputfile", dest="outputfilename", help="write to OUTFILE", metavar="OUTFILE")
#parser.add_option("-d", "--difffile", dest="difffilename", help="write to DIFFFILE", metavar="DIFFFILE")
#parser.add_option("-a", "--allcalled", dest="allcalled", action="store_true", help="Only use positions called in all strains")
(options, args) = parser.parse_args()
#
# Open files
#
#
# File with reads
#
if options.inputfilename != None:
  inputfile = open(options.inputfilename,"r")
else:
  inputfile = sys.stdin
#
# File for general output
#
if options.outputfilename != None:
  outputfile = open(options.outputfilename,"w")
else:
  outputfile = sys.stdout
#
# File for differences between samples
#
#if options.difffilename != None:
#  difffile = open(options.difffilename,"w")
#
# Read Input fasta file
#
inputseq = []
inputseqsegments = []
consensusseq = []
inputname = []
inputdesc = []
Ninputs=0
i=0
#if options.inputfilename != None:
if 1 != 0:
  # allways true
  t1 = time.time()
  #sys.stdout.write("%s %d %s\n" % ("# Time used: ", int(t1-t0)," seconds"))
  #sys.stdout.write("%s\n" % ("# Reading inputfile"))
  for line in inputfile:
    fields=line.split()
    if len(line)>1:
      if fields[0][0] == ">":
        #print "# input ",line
	if (i>0):
	  inputseq[-1] = ''.join(inputseqsegments)
	  #print len(inputseq),len(inputseq[-1]),len(inputseq[len(inputseq)-1])
        del inputseqsegments
        inputseqsegments = []
	i=0
        inputseq.append("")
        consensusseq.append("")
        inputname.append(fields[0][1:])
        inputdesc.append(re.sub(r"^[^\s]+\s","",line.strip()))
      else:
        #inputseq[-1] = inputseq[-1]  + fields[0]
	#print i
	#print fields[0]
        inputseqsegments.append("")
        inputseqsegments[i] = fields[0]
	i+=1
  inputseq[-1] = ''.join(inputseqsegments)
  #print len(inputseq),len(inputseq[-1]),len(inputseq[len(inputseq)-1])
del inputseqsegments

#
#
#
nseq = len(inputseq)
readlength = 150
stepsize = 5
coverage=30
#
#
#
for i in xrange(0, nseq):
  start = 0
  end = start + readlength
  while (start + readlength < len(inputseq[i])):
    outputfile.write("%s\n" % ("@"))
    outputfile.write("%s\n" % (inputseq[i][start:end]))
    outputfile.write("%s\n" % ("+"))
    curreadlength = len(inputseq[i][start:end])
    for j in xrange(0,curreadlength):
      outputfile.write("%s" % ("6"))
    outputfile.write("\n")
    start += stepsize
    end += stepsize
  if (len(inputseq[i])>readlength):
    for k in(0,coverage):
      #
      # make sure to get coverage also in the ends (the overlapping windows
      # tapper off in the ends)
      #
      end = len(inputseq[i])
      outputfile.write("%s\n" % ("@"))
      outputfile.write("%s\n" % (inputseq[i][0:readlength]))
      outputfile.write("%s\n" % ("+"))
      for j in xrange(0,readlength):
        outputfile.write("%s" % ("6"))
      outputfile.write("\n")
      outputfile.write("%s\n" % ("@"))
      outputfile.write("%s\n" % (inputseq[i][end-readlength:end]))
      outputfile.write("%s\n" % ("+"))
      for j in xrange(0,readlength):
        outputfile.write("%s" % ("6"))
      outputfile.write("\n")


#
# Close files
#
#t1 = time.time()
#sys.stdout.write("%s %d %s\n" % ("# Finishing. Time used: ", int(t1-t0)," seconds"))
#inputfile.close()
#templatefile.close()
#outputfile.close()
