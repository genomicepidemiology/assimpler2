assimpler scripts
========

# What is it?


The assimpler folder contains the scripts *assimpler.py*, *chnam.py*,
*mummer2aln.py*, *nctree.py*, *stripali.py*, *NDtree.py*

* *assimpler.py* is a simple method to map reads to a template.

* *chman.py* is a utility script to change names of fasta files to short unique
names, and back to the original names.

* *mummer2aln.py* is a wrapper arround mummer programs to align a an assembled
genome or contigs in fata format to a refference genome

* *nctree.py* is used to create a distance matrix from a set of alligned fasta
files. These may be million bases long mappings made for example by the assimpler.py
script.

* *stripali.py* is a script to remove all positions in a set op aligned fasta files
that er either fully conserved or where one of the sequences contain a non
[AGTA] charecter (for example "N").

* *NDtree.py* is a wrapper automating creating a phylogenetic tree from sets of
reads using the above programs, and the neighbor program from the phylip
package. It is used to run the NDtree (http://cge.cbs.dtu.dk/services/NDtree/)
service. It creates a number of temporary files and should be run in an empty
directory.

Usage
-----

 All the programs can be invoked with the -h option to get help:

 assimpler.py -h
```bash
Usage: assimpler.py [options]

Options:
  -h, --help            show this help message and exit
  -i INFILE, --inputfile=INFILE
                        read from INFILE
  -t TEMFILE, --templatefile=TEMFILE
                        read from TEMFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -s COLUMN, --columnfile=COLUMN
                        write to column file
  -c CONFILE, --consensusfile=CONFILE
                        write fasta assembly to CONFILE
  -n ENTRYNAME, --entryname=ENTRYNAME
                        Set name in fastafile to ENTRYNAME
  -u UNUSEDFILE, --unusedfile=UNUSEDFILE
                        write to UNUSEDFILE
  -m MINCOVERAGE, --mincoverage=MINCOVERAGE
                        lower bound of coverage for assembly to print out.
                        Dekault is 0 (~1 read )
  -a MAXALTCALLS, --maxlatcalls=MAXALTCALLS
                        maximum fraction of alternative calls. Default is 0.1
  -z MINZSCORE, --minzscore=MINZSCORE
                        Minimum Z-score to call bases. Deafault is 3.29 ~ p =
                        0.001
  -M KMERLENGTH, --mismatchscore=KMERLENGTH
                        Mismatch score (penalty). Deafault is 3
  -k MISMATCHSCORE, --kmerlength=MISMATCHSCORE
                        k-mer length to use in hash table. default is 17
  -l MINSCORE, --minscore=MINSCORE
                        Minscore to accept mapping. Deafault is 50
  -f, --frogdna         Use template DNA in regions with no reads
  -e, --extendtemplate  extend templates if reads overhang in the end/create
                        new templat if no match is found
  -d, --diploid         Make base calls for diploid genomes
```

chnam.py -h
```bash
Usage: chnam.py [options]

Options:
  -h, --help            show this help message and exit
  -i INFILE, --inputfile=INFILE
                        read from INFILE
  -s SUBFILE, --substitutionfile=SUBFILE
                        read from SUBFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -t OUTTYPE, --outputtype=OUTTYPE
                        Output type 1: Tree, 2: tree type 2, 3: Species finder
```

mummer2aln.py -h
```bash
 usage: mummer2aln.py [options]

 Pseudo-global alignment using mummer of contigs vs. reference, then align
 using muscle to give contig-reference alignment

 optional arguments:
  -h, --help  show this help message and exit
  --i I       input sequence file
  --ref REF   input reference file
  --o O       output alignment
```

NB. This script have hardcoded paths to mummer programs and use a different path to the python installation.




nctree.py -h
```bash
Usage: nctree.py [options]

Options:
  -h, --help            show this help message and exit
  -i INFILE, --inputfile=INFILE
                        read from INFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -d DIFFFILE, --difffile=DIFFFILE
                        write to DIFFFILE
  -a, --allcalled       Only use positions called in all strains
```


stripali.py -h
```bash
Usage: stripali.py [options]

Options:
  -h, --help            show this help message and exit
  -i INFILE, --inputfile=INFILE
                        read from INFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -d DIFFFILE, --difffile=DIFFFILE
```

NDtree.py -h
```bash
Usage: NDtree.py [options]

Options:
  -h, --help            show this help message and exit
  -f FILEFILE, --filefile=FILEFILE
                        read from FILEFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -t TEMPLATEFILE, --templatefile=TEMPLATEFILE
                        read from TEMPLATEFILE
  -n LINESPERISOLATE, --linesperisolate=LINESPERISOLATE
                        Number of lines per isolate LINESPERISOLATE (two for
                        paired ends)
  -w, --web             Run in web server mode
  -s, --sim             Create sim file with collumns of base frequencies
  -p PATH, --path=PATH  path to be added to file names in FILEFILE
  -z MINZSCORE, --minzscore=MINZSCORE
                        Minimum Z-score to call bases. Deafault is 3.29 ~ p =
                        0.001
  -a, --allcalled       Only use positions called in all strains
  -k KMERLENGTH, --kmerlength=KMERLENGTH
                        k-mer length to use in hash table. default is 17
  -l MINSCORE, --minscore=MINSCORE
                        Minscore to accept mapping. Deafault is 50
```
## Installation

```bash
# Go to wanted location
cd /path/to/some/dir
# Clone and enter the assimpler directory
hg clone https://bitbucket.org/genomicepidemiology/assimpler.git
cd assimpler
```
Change the hardcoded path in NDtree.py to the install directory of assimpler:
```bash
binpath = "/path/to/some/dir/assimpler"
```

## Example of use

Make list of fastq file paths, where forward and reverse reads for the same sample are space separated on the same line, for example:
```bash
ls -1 *.fastq.gz | gawk '{if (/_R1[\._]/||/_1[\._]/){printf("%s ",$0)}else {print $0}}'  > isolates.list
```

To also include the reference genome in the phylogenic tree, add to list:
```bash
echo "reference_genome.fsa" >> isolates.list
```

Make tree:

```bash
/path/to/some/dir/assimpler/NDtree_p.py \
-f isolates.list \
-t reference_genome.fsa \
-z 1.96 -k 17 -a
```
-f is the file containing the list of raw reads, -t is the template for the consensus sequences, -k is the k-mer size (17 for bacteria and 31 for malaria), -z is the Z-score threshold for base-calling (corresponding to a p-value of 0.05), and -a means that only those positions are used in the distance calculation, where none of the sequences have ambiguous bases.

## Web-server

A webserver implementing the methods is available at:

http://cge.cbs.dtu.dk/services/NDtree/


## The Latest Version

The latest version can be found at
https://bitbucket.org/genomicepidemiology/assimpler/overview.


## Documentation

The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/assimpler/overview.

## Installation

The scripts are self contained. You just have to copy them to where they should
be used. The scripts are dependend on a number of python packages:

* Bio
* argparse
* array
* gc
* numpy
* operator
* optparse

The scripts are further dependend on the mummer package
(http://mummer.sourceforge.net) and the neighbor program from the phylip
package (http://evolution.genetics.washington.edu/phylip.html).
Some hard links may need to be changed especially in the mummer2aln.py script.

The scripts can only be invoked from within the folder unless you add the folder to your system path.

## Citation

When using the method please cite:

Evaluation of whole genome sequencing for outbreak detection of Salmonella
enterica. Leekitcharoenphon P, Nielsen EM, Kaas RS, Lund O, Aarestrup FM.
PLoS One. 2014 Feb 4;9(2):e87991
PMID: 24505344

Real-Time Whole-Genome Sequencing for Routine Typing, Surveillance, and
Outbreak Detection of Verotoxigenic Escherichia coli. Joensen KG, Scheutz F,
Lund O, Hasman H, Kaas RS, Nielsen EM, Aarestrup FM. J Clin Microbiol. 2014
May;52(5):1501-10.
PMID: 24574290

## Licensing

Please see the file called LICENSE.
