#!/usr/bin/env python
#
# Import libraries
#
import sys, time
import os
#from math import sqrt
from optparse import OptionParser
#from operator import itemgetter
#import re
#import cPickle as pickle
#import bsddb3 as bsddb
#import anydbm
#import shelve
#
# Functions
#
# Parse command line options
#
parser = OptionParser()
parser.add_option("-i", "--inputfile", dest="inputfilename", help="read from INFILE", metavar="INFILE")
parser.add_option("-s", "--substitutionfile", dest="substitutionfilename", help="read from SUBFILE", metavar="SUBFILE")
parser.add_option("-o", "--outputfile", dest="outputfilename", help="write to OUTFILE", metavar="OUTFILE")
parser.add_option("-t", "--outputtype", dest="outputtype", help="Output type 1: Tree, 2: tree type 2, 3: Species finder", metavar="OUTTYPE")
(options, args) = parser.parse_args()
#
# Open files
#
if options.inputfilename != None:
  if options.inputfilename == "--":
    inputfile = sys.stdin
  else:
    inputfile = open(options.inputfilename,"r")
#
if options.substitutionfilename != None:
  substitutionfile = open(options.substitutionfilename,"r")
else:
  sys.exit("No substitution file specified")
#
if options.outputfilename != None:
  outputfile = open(options.outputfilename,"w")
else:
  outputfile = sys.stdout
#
#
#
if options.outputtype != None:
  outputtype = options.outputtype
  #print "A",outputtype
else:
  outputtype = 1
  #print "B",outputtype
#
# Read substitution file
#
substitutions = {}
substitutions2 = {}
#outputfile.write("%s\n" % ("# Reading database of substitutions"))
i = 0
for line0 in substitutionfile:
  line = line0.strip()
  #line.strip()
  fields=line.split("\t")
  subfields = fields[1].split()
  entries = fields[0].split()
  #substitutions[fields[0]] = fields[1]
  for entry in entries:
    i = i+1
    tmpname = " SEQ_NFOPIDBFKDJFRBGVODVNJVUGDH_" + str(i) + "_"
    if outputtype == "1":
      #print "C", outputtype
      substitutions[entry] = '"' + fields[1] + " " + tmpname + '"'
      substitutions2[tmpname] = entry
    elif outputtype == "2":
      #print "C", outputtype
      substitutions[entry] = fields[1] +  tmpname
      substitutions2[tmpname] = ""
    else:
      substitutions[entry] = subfields[0] + " " + subfields[1] + " " + tmpname
      substitutions2[tmpname] = entry
      #print "D", outputtype
#
#
#
#
# Read inputfile
#
inputlines = []
i=0
if options.inputfilename != None:
  #sys.stdout.write("%s\n" % ("# Reading inputfile"))
  for line in inputfile:
    #print "XXX",line0,"YYY"
    #line = line0.strip()
    #print "VVV",line,"WWW"
    inputlines.append("")
    inputlines[i] = line
    i+=1
inputstring = ''.join(inputlines)

if outputtype == "3":
  outputfile.write("Genus Species Match #hits\n")

for name in substitutions:
  #print name,"XXX",substitutions[name]
  inputstring = inputstring.replace(name, substitutions[name])
#outputfile.write("%s\n" % (inputstring))

for name in substitutions2:
  #print name,"YYY",substitutions2[name]
  inputstring = inputstring.replace(name, substitutions2[name])

#print inputstring
outputfile.write("%s" % (inputstring))
#
#
#
#sys.stdout.flush()
#inputfile.close()
#substitutionfile.close()
#outputfile.close()
