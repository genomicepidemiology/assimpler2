#!/usr/bin/env python

# Copyright (c) 2014, Ole Lund, Technical University of Denmark
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Import libraries
#
import sys, time
import os
import gc
import numpy as np
import array
from optparse import OptionParser
from operator import itemgetter
import re
#import gzip
#
# Functions
#
# Start time to keep track of progress
#
t0 = time.time()
#
#
#
etta = 0.001
#
# Parse command line options
#
parser = OptionParser()
parser.add_option("-i", "--inputfile", dest="inputfilename", help="read from INFILE", metavar="INFILE")
parser.add_option("-o", "--outputfile", dest="outputfilename", help="write to OUTFILE", metavar="OUTFILE")
parser.add_option("-d", "--difffile", dest="difffilename", help="write to DIFFFILE", metavar="DIFFFILE")
parser.add_option("-a", "--allcalled", dest="allcalled", action="store_true", help="Only use positions called in all strains")
(options, args) = parser.parse_args()
#
# Open files
#
#
# File with reads
#
if options.inputfilename != None:
  inputfile = open(options.inputfilename,"r")
else:
  inputfile = sys.stdin
#
# File for general output
#
if options.outputfilename != None:
  outputfile = open(options.outputfilename,"w")
else:
  outputfile = sys.stdout
#
# File for differences between samples
#
if options.difffilename != None:
  difffile = open(options.difffilename,"w")
#
# Read Input fasta file
#
inputseq = []
inputseqsegments = []
consensusseq = []
inputname = []
inputdesc = []
Ninputs=0
i=0
#if options.inputfilename != None:
if 1 != 0:
  # allways true
  t1 = time.time()
  #sys.stdout.write("%s %d %s\n" % ("# Time used: ", int(t1-t0)," seconds"))
  sys.stdout.write("%s\n" % ("# Reading inputfile"))
  for line in inputfile:
    fields=line.split()
    if len(line)>1:
      if fields[0][0] == ">":
        #print "# input ",line
	if (i>0):
	  inputseq[-1] = ''.join(inputseqsegments)
	  #print len(inputseq),len(inputseq[-1]),len(inputseq[len(inputseq)-1])
        del inputseqsegments
        inputseqsegments = []
	i=0
        inputseq.append("")
        consensusseq.append("")
        inputname.append(fields[0][1:])
        inputdesc.append(re.sub(r"^[^\s]+\s","",line.strip()))
      else:
        #inputseq[-1] = inputseq[-1]  + fields[0]
	#print i
	#print fields[0]
        inputseqsegments.append("")
        inputseqsegments[i] = fields[0]
	i+=1
  inputseq[-1] = ''.join(inputseqsegments)
  #print len(inputseq),len(inputseq[-1]),len(inputseq[len(inputseq)-1])
del inputseqsegments

#
# Cast sequences in numpy datastructure
#
nseq = len(inputseq)
lseq0 = len(inputseq[0])
nchrom=1
lseqmax = lseq0
while (len(inputseq[nchrom]) != lseq0):
  sys.stdout.write("# Length of chromosome %d: %d\n" % (nchrom,len(inputseq[nchrom-1])))
  if (len(inputseq[nchrom]) > lseqmax):
    lseqmax = len(inputseq[nchrom])
  nchrom +=1
sys.stdout.write("# Length of chromosome %d: %d\n" % (nchrom,len(inputseq[nchrom-1])))
nstrain = int(nseq/nchrom)
sys.stdout.write("# Number of strains: %d\n" % (nstrain))
sys.stdout.write("# Number of chromosomes: %d\n" % (nchrom))
#for h in xrange(0, nchrom):
inputseqmat = np.zeros ((nseq,lseqmax), dtype=np.int8)
non_nucleotidemat = np.zeros ((nseq,lseqmax), dtype=np.int8)

#
# Define nucleotides as numbers
#
nuc2num = {
  #
  # A  adenosine          C  cytidine             G  guanine
  # T  thymidine          N  A/G/C/T (any)        U  uridine
  # K  G/T (keto)         S  G/C (strong)         Y  T/C (pyrimidine)
  # M  A/C (amino)        W  A/T (weak)           R  G/A (purine)
  # B  G/T/C              D  G/A/T                H  A/C/T
  # V  G/C/A              -  gap of indeterminate length
  #   A C G T
  # A A M R W
  # C M C S Y
  # G R S G K
  # T W Y K T
  #
  'A' : 1,
  'T' : 2,
  'C' : 3,
  'G' : 4,
  'M' : 5,
  'R' : 6,
  'W' : 7,
  'S' : 8,
  'Y' : 9,
  'K' : 10
}
#
# Cast sequences as numpy vectors
#
t1 = time.time()
sys.stdout.write("%s %d %s\n" % ("# Time used: ", int(t1-t0)," seconds"))
sys.stdout.write("%s\n" % ("# Cast input sequences in numpy"))
#
# Find non nucleotide positions
#
if options.allcalled != None:
  for i in xrange(0, nseq):
    for j in xrange (0,len(inputseq[i])):
      try:
        # Set A, T, C, G to 1, 2, 3, 4, respectively (vector is initialized to 0)
        inputseqmat[i][j] = nuc2num[inputseq[i][j]]
      except:
        # Set to 1 if position do not contain an A, T, C or G (vector is initialized to 0)
        non_nucleotidemat[i][j] = 1
#
# Keep only positions which are called in all sequences - this code is commented out
#
for n in xrange(0, nchrom):
  bad = 0
  sys.stdout.write("# Chromosome: %d\n" % (n+1))
  for j in xrange (0,len(inputseq[n])):
    #print "pos ",j
    for l in xrange(0, nstrain):
      #print "strain ",l
      i = n+l*nchrom
      try:
        # Set A, T, C, G to 1, 2, 3, 4, respectively (vector is initialized to 0)
        inputseqmat[i][j] = nuc2num[inputseq[i][j]]
      except:
        # Set to 1 if position do not contain an A, T, C or G (vector is initialized to 0)
        non_nucleotidemat[i][j] = 1
	#
	# if options.extendtemplate != None:
	#
	if options.allcalled != None:
	  #print n,j,l, inputseq[i][j]
	  bad += 1
          for l in xrange(0, nstrain):
	    i = n+l*nchrom
	    inputseqmat[i][j] = 0
	    non_nucleotidemat[i][j] = 1
	  break
  if options.allcalled != None:
    sys.stdout.write("# Number of positions used for phylogeny in chromosome %d: %d\n" % (n+1,len(inputseq[n])-bad))

#print bad

#
# Calculate pairwise distances
#
t1 = time.time()
sys.stdout.write("%s %d %s\n" % ("# Time used: ", int(t1-t0)," seconds"))
sys.stdout.write("%s\n" % ("# Calculating pairwise distances"))
mat = np.zeros ((nstrain,nstrain))
prog=0


if options.difffilename == None:
  #
  # fast calculation not saving different positions
  #
  for l in xrange(0, nstrain):
    for m in xrange(0, l):
      for n in xrange(0, nchrom):
        i = l*nchrom+n
        j = m*nchrom+n
        #for j in xrange(0, i):
        prog +=1
	dist = np.sum(inputseqmat[i]!=inputseqmat[j])-np.sum(non_nucleotidemat[i]!=non_nucleotidemat[j])
	#calledi = np.sum(inputseqmat[i]!=0)
	#calledj = np.sum(inputseqmat[j]!=0)
	#chromlength = len(inputseq[i])
	#
	# Estimate genetic distance if all bases had been called in both sequences
	#
	#scalefactor = float((chromlength*chromlength)/(calledi*calledj+0.001))
	#print "l: ",l,"m: ",m,"n: ",n,"dist: ",dist,"mat: ",mat[l][m]
        mat[l][m] += dist
        #mat[l][m] += dist*scalefactor
        mat[m][l] = mat[l][m]
	#print "l: ",l,"m: ",m,"i: ",i,"n: ",n,"j: ",j, "dist: ", dist, "sumdist: ", mat[l][m], "Calledi: ", calledi, "calledj: ",calledj, "len: ", len(inputseq[i]), "dist: ", dist, "scalefactor: ", scalefactor,"mat: ", mat[l][m]
    #sys.stdout.write("\r# %s%s done" % (int(2*100*prog/(nseq*nseq+0.001)),"%"))
    #sys.stdout.flush()
  #sys.stdout.write("\n")


else:
  #
  # Slow version keeping information about different positions
  # NB this has to be reworked to handle multi chromosome genomes
  #
  for i in xrange(0, nseq):
    for j in xrange(0, i):
      prog +=1
      sum = 0
      for k in xrange(0, lseq):
        if (inputseqmat[i][k]!=inputseqmat[j][k] and inputseqmat[i][k] > 0 and inputseqmat[j][k] > 0):
	  sum += 1
	  difffile.write("%-8s %-8s %8d %1s %1s\n" % (inputname[i][0:8],inputname[j][0:8], k+1, inputseq[i][k], inputseq[j][k]))
      mat[i][j] = sum
      mat[j][i] = mat[i][j]
      sys.stdout.write("\r# %s%s done" % (int(2*100*prog/(nseq*nseq+0.001)),"%"))
      sys.stdout.flush()
  sys.stdout.write("\n")


#
# Write in neighbor format
#
sys.stdout.write("%s\n" % ("# Writing output in neighbor format"))
outputfile.write("%s\n" % (nstrain))
for i in xrange(0,nstrain):
  mynamei = inputname[i*nchrom][0:8]
  outputfile.write("%-11s " % (mynamei))
  for j in xrange(0,nstrain):
    mynamej = inputname[i*nchrom][0:8]
    outputfile.write("%0.8f" % (mat[i][j]))
    if ((j+1) % 6 == 0) or (j == nstrain-1):
      outputfile.write("\n")
    else:
      outputfile.write(" ")
#
# Close files
#
t1 = time.time()
sys.stdout.write("%s %d %s\n" % ("# Finishing. Time used: ", int(t1-t0)," seconds"))
#inputfile.close()
#templatefile.close()
#outputfile.close()
