#!/tools/bin/python2.7
# #!/panvol1/simon/bin/python2.7

import argparse
import subprocess
import os
import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import re

nucmer = '/usr/local/bin/nucmer'
deltafilter = '/usr/local/bin/delta-filter'
showcoords = '/usr/local/bin/show-coords'
showaligns = '/usr/local/bin/show-aligns'

def mummeraln(i, ref, o):
   '''Align using mummer and extract sequences'''

   # Run alignments and parse to coords
   contigs = i
   f = os.path.split(contigs)[1]

   # nucmer
   cmd = nucmer + ' -maxmatch -c 100 -p %s %s %s' % (f, ref, contigs)
   ec = subprocess.call(cmd, shell=True)

   # delta-filter
   gdelta = f +'.global.delta'
   cmd = deltafilter + ' -l 100 -i 50 -q %s.delta > %s' % (f, gdelta)
   ec = subprocess.call(cmd, shell=True)

   # show-coords
   cmd = showcoords + ' -r -T -c %s.global.delta' % (f)
   p_coords = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
   coords = p_coords.communicate()[0].split('\n')
   coords.remove('')
   coords.remove('')

   # parse coords, extract alignments and add padding
   final_aln = parse_aligns(coords, gdelta, contigs, ref)

   sys.stderr.write('Writing alignment ')
   # create and write new SeqRecord
   final_seq = SeqRecord(Seq(''.join(final_aln).upper()))
   final_seq.id = os.path.split(contigs)[1]
   final_seq.description = 'mummer pseudo-global aln'

   fh_out = open(o, 'w')
   c = SeqIO.write(final_seq, fh_out, 'fasta')
   fh_out.close()
   sys.stderr.write('done!\n')


def parse_aligns(coords, delta, contigs, ref):
   '''Parse coords, create aligns files, and add padding'''

   reg_begin = re.compile('-- BEGIN alignment \[ (.)1 (\d+) - (\d+) \| (.)1 (\d+) - (\d+) \]')
   reg_seq = re.compile('^\d+\s+(.+)')

   # open input files
   contigs_seq = SeqIO.to_dict(SeqIO.parse(open(contigs, 'r'), 'fasta'))
   ref_seq = SeqIO.to_dict(SeqIO.parse(open(ref, 'r'), 'fasta'))

   # calc length of reference
   ref_len = 0
   for key in ref_seq.keys():
      ref_len += len(ref_seq[key].seq)

   sys.stderr.write('Parsing alignments\n')

   # first pass of coords file to generate dict of showaligns
   showaln = {}
   for i,line in enumerate(coords):
      if i < 2: pass
      elif i == 2:
         header = line.split('\t')
      else:
         #if i%50 == 0: sys.stderr.write('%i  ' % (i))
         fields = line.split('\t')

         ref_contig = (fields[9], fields[10])
         if showaln.has_key(ref_contig): pass
         else: showaln[ref_contig] = []

   # parse the showalns and create dict where key are pos+seqnames and value are sequence
   # could be multi-threaded
   aln_dict = {}
   for key in showaln.keys():
      cmd = showaligns + ' -w 1000 %s "%s" "%s"' % (delta, key[0], key[1])
      p_aligns = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
      stdout = p_aligns.communicate()[0].split('\n')
      stdout.remove('')

      # parse through showaln to index it
      for j,line2 in enumerate(stdout):
         if j < 4: pass

         # find alignment begins #
         m = reg_begin.search(line2)
         if m:
            seqs = []
            r_frame = m.group(1)
            r_start = int(m.group(2))
            r_end = int(m.group(3))
            c_frame = m.group(4)
            c_start = int(m.group(5))
            c_end = int(m.group(6))

         # find sequences
         m_seq = reg_seq.search(line2)
         if m_seq: seqs.append(m_seq.group(1))

         # find ends
         if line2.startswith('--   END alignment'):

            # get the sequences
            r_seq = []
            c_seq = []
            for j in range(len(seqs)):
               if j%2 == 0: r_seq.append(seqs[j])
               else: c_seq.append(seqs[j])

            # convert to string
            r_seq = ''.join(r_seq)
            c_seq = ''.join(c_seq)

            # search through sequences to find deletions/insertions
            # gap-stripping and replace '.' with '-' from contig aln
            new = []
            for n,base in enumerate(r_seq):
               if base == '.': pass
               else: new.append(c_seq[n])

            new_seq = ''.join(new)
            new_seq = new_seq.replace('.', '-')

            # add new_seq to dict
            k = (key[0], key[1], r_start, r_end, c_start, c_end)
            aln_dict[k] = new_seq


   # parse through coords (second pass) and get the sequences of the hits
   alignment = {}
   for i,line in enumerate(coords):
      if i < 2: pass
      elif i == 2:
         header = line.split('\t')
      else:
         #if i%50 == 0: sys.stderr.write('%i  ' % (i))
         fields = line.split('\t')

         # set frame
         frame = ''
         if int(fields[2]) > int(fields[3]): frame = '-'
         else: frame = '+'
         ref_start = int(fields[0])
         ref_end = int(fields[1])
         contig_start = int(fields[2])
         contig_end = int(fields[3])
         contig_id = fields[10]
         ref_id = fields[9]

         # search and add to alignment dict
         key = (fields[9], fields[10], ref_start, ref_end, contig_start, contig_end)
         id = (ref_start, ref_end)
         alignment[id] = aln_dict[key]

   sys.stderr.write('done!\n')
   sys.stderr.write('Creating global alignment\n')

   # create global alignment #
   # create list of tuples [((start, end), 'actg'), ...]
   sortedlist = [(k, alignment[k]) for k in sorted(alignment)]

   # then add padding
   final_aln = []
   prev = 0
   sortedlist_len = len(sortedlist)
   # t = sortedlist[1]
   for j,t in enumerate(sortedlist):

      if j == 0: ref_start = t[0][0]
      else: ref_start = t[0][0]

      # add padding
      diff = ref_start-prev
      if diff == 0:
         prev = t[0][1]
         if j == 0:
            final_aln.append(t[1])
         else:
            final_aln.append(t[1][1:])
      elif diff > 0:
         # add padding
         final_aln.append('-'*(diff-1))
         # add sequence
         final_aln.append(t[1])
         prev = t[0][1]
      else:
         # if overlapping alignments cut out of next segment
         if abs(diff) < len(t[1]):
            final_aln.append(t[1][(abs(diff)+1):])
            prev = t[0][1]

   # check if padding should be added to the final step
   aln_len = len(''.join(final_aln))
   if aln_len < ref_len:
      diff = ref_len - aln_len
      final_aln.append('-'*(diff))
      aln_len = len(''.join(final_aln))
      # check again
      if aln_len != ref_len:
         sys.stderr.write('Error in creating final alignment\n')
   elif aln_len == ref_len:
      pass
   else:
      sys.stderr.write('Error in creating final alignment, alignment length is larger than reference\n')

   # return alignment
   return final_aln


if __name__ == '__main__':

   # create the parser
   parser = argparse.ArgumentParser(prog='mummer2aln.py', formatter_class=lambda prog: argparse.HelpFormatter(prog,max_help_position=50, width=130), usage='%(prog)s [options]', description='''Pseudo-global alignment using mummer of contigs vs. reference, then align using muscle to give contig-reference alignment''')

   parser.add_argument('--i', help='input sequence file', required=True)
   parser.add_argument('--ref', help='input reference file', required=True)
   parser.add_argument('--o', help='output alignment', required=True)

   args = parser.parse_args()
   #args = parser.parse_args('--i S_thermohpilus.contigs.fa --ref S_thermophilus_CNRZ1066.fasta'.split())
   #args = parser.parse_args('--i 13146.fna --ref /panfs1/cge-servers/CGE/CGE-1.0/database/Escherichia_coli.ref --o 13146.ecoli.fna'.split())
   #args = parser.parse_args(' --i /panfs1/cge-servers/CGE/CGE-1.0/database/rolf_database/41499.fna --ref /panfs1/cge-servers/CGE/CGE-1.0/database/Escherichia_coli.ref --o 41499.aln'.split())


   mummeraln(args.i, args.ref, args.o)
