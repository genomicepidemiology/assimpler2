#!/usr/bin/env python

# Copyright (c) 2014, Ole Lund, Technical University of Denmark
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Import libraries
#
import sys
#import time
import os
#import gc
#import numpy as np
#import array
from optparse import OptionParser
#from operator import itemgetter
#import re
#import gzip
#
# Functions
#
# Start time to keep track of progress
#
#t0 = time.time()
#
#
#
etta = 0.001
#
# Set up path depending on computer
#
# Path at local computer
binpath = "~/bin/"
dbpath = "~/projects/data/kmer/"
#
# Path at cgebase
#binpath = "/home/data1/services/NDtree/NDtree-1.0/Scripts/"
#dbpath = "/home/data1/services/NDtree/NDtree-1.0/Scripts/"
#
# Path at cge-s2
#binpath = "/panfs1/cge/people/lund/bin/"
#dbpath = "/panfs1/cge/people/lund/db/"

#
# Parse command line options
#
parser = OptionParser()
parser.add_option("-f", "--filefile", dest="filefilename", help="read from FILEFILE", metavar="FILEFILE")
parser.add_option("-o", "--outputfile", dest="outputfilename", help="write to OUTFILE", metavar="OUTFILE")
parser.add_option("-t", "--templatefile", dest="templatefilename", help="read from TEMPLATEFILE", metavar="TEMPLATEFILE")
parser.add_option("-n", "--linesperisolate", dest="linesperisolate", help="Number of lines per isolate LINESPERISOLATE (two for paired ends)", metavar="LINESPERISOLATE")
parser.add_option("-w", "--web", dest="web", action="store_true", help="Run in web server mode")
parser.add_option("-s", "--sim", dest="sim", action="store_true", help="Create sim file with collumns of base frequencies")
parser.add_option("-p", "--path", dest="path", help="path to be added to file names in FILEFILE", metavar="PATH")
parser.add_option("-z", "--minzscore", dest="minzscore", help="Minimum Z-score to call bases. Deafault is 3.29 ~ p = 0.001", metavar="MINZSCORE")
parser.add_option("-a", "--allcalled", dest="allcalled", action="store_true", help="Only use positions called in all strains")
parser.add_option("-d", "--diploid", dest="diploid", action="store_true", help="Make base call for diploid genomes")
parser.add_option("-k", "--kmerlength", dest="kmerlength", help="k-mer length to use in hash table. default is 17", metavar="KMERLENGTH")
parser.add_option("-l", "--minscore", dest="minscore", help="Minscore to accept mapping. Deafault is 50", metavar="MINSCORE")
(options, args) = parser.parse_args()
#
# Open files
#
#
# File with files with reads
#
if options.filefilename != None:
  #if (fileExtension == ".gz"):
  #  filefile = gzip.open(options.filefilename,"r")
  #else:
  filefile = open(options.filefilename,"r")
else:
  filefile = sys.stdin
#
# File for general output
#
if options.outputfilename != None:
  outputfile = open(options.outputfilename,"w")
else:
  outputfile = sys.stdout
#
# Path
#
if options.path != None:
  filepath = options.path
else:
  filepath = ""
#
# Number of files per isolate
#
if options.linesperisolate != None:
  linesperisolate = int(options.linesperisolate)
else:
  linesperisolate = 1
#
# Make output look like plain text in html
#
outputfile.write("# <pre>")


#
# Read file with file names
#
Nisolates = 0
isolatefiles = []
line3 = ""
linesforthisisolate=0
for line in filefile:
  if len(line)>1:
    line2 = line.rstrip()
    linesforthisisolate += 1
    if options.path != None:
      fields=line.split()
      for field in fields:
        line3 = line3 + " " + filepath + "/" + field + " "
    else:
      line3 = line3 + " " + line2
    if (line3[-1] == "a" or (linesforthisisolate % linesperisolate == 0)):
      isolatefiles.append("")
      isolatefiles[Nisolates] = line3
      outputfile.write("# Isolate_No: %s Isolate_Files: %s\n" % (Nisolates+1, isolatefiles[Nisolates]))
      line3 = ""
      linesforthisisolate = 0
      Nisolates += 1
#
# Get template
#
besthitfile = "besthit.fsa"
if options.templatefilename == None:
  #
  # Find best hit
  #

  #
  # Se if files are compressed or not (all or none are assumed to be compressed in each line)
  #
  compressed = 0
  #print "XX",isolatefiles[0][-2],"ZZ"
  if isolatefiles[0][-2] == "z" or isolatefiles[0][-1] == "z":
    compressed = 1
  #
  # Find best hit of first file usisng kmerfinder
  #
  findtemplateoutputfile = "findtemplate.out"
  findtemplatelogfile = "findtemplate.log"
  if compressed:
    cmd = "gunzip -c " + isolatefiles[0] + " | " + binpath + "/findtemplate.py -i -- -t " + dbpath + "complete_genomes.ATGAC -xATGAC -o " + findtemplateoutputfile + " > " + findtemplatelogfile
  else:
    cmd = "cat " + isolatefiles[0] + " | " + binpath + "/findtemplate.py -i -- -t " + dbpath + "complete_genomes.ATGAC -xATGAC -o " + findtemplateoutputfile + " > " + findtemplatelogfile
  #outputfile.write("# %s\n" % (cmd))
  os.system(cmd)
  #outputfile.write("# Best hit from findtemplate output\n")
  tmpfile = open("findtemplate.out","r")
  i=0
  for line in tmpfile:
    #outputfile.write("%s" % (line))
    i+=1
    if i == 2:
      break
  tmpfile.close()
  #
  # Get fastafile for best hit
  #
  besthit = ""
  filefile = open(findtemplateoutputfile,"r")
  Nhits = 0
  hits = []
  for line in filefile:
    if len(line)>1 and line[0][0] != "#":
      hits.append("")
      hits[Nhits] = line.rstrip()
      if (Nhits == 1):
        fields=line.split()
        besthit = fields[0]
      Nhits += 1
  outputfile.write("#\n")
  outputfile.write("# Template: %s\n" % (besthit))
  outputfile.write("#\n")
  besthitfile = "besthit.fsa"
  cmd = "echo " + besthit + " | " + binpath + "/getfasta.py -i " + dbpath + "complete_genomes.fsa -n -- > " + besthitfile
  #outputfile.write("# %s\n" % (cmd))
  os.system(cmd)
else:
  #
  # Get user supplied template, and change nucleotides to upper case
  #
  cmd = "gawk '{if (/^>/){print $0}else {print toupper($0)}}' " + options.templatefilename + " > " + besthitfile
  os.system(cmd)
#
# Run assimpler (this may be paralellized to speed up things)
#
namefile = open("names","w")
os.system("touch map.fsa")
os.system("rm map.fsa")
i=1
for isolate in isolatefiles:
  # Change names so nothing goes wrong with them (some programs don't like funny names!)
  isolatename = "I" + '{:05}'.format(i)
  isolatefilenames = isolate.split()
  isolatename2 = isolatefilenames[0]
  isolatename2 = os.path.basename(isolatename2)
  isolatename2 = os.path.splitext(isolatename2)[0]
  #outputfile.write("# Isolatename: '%s' Isolate_short_name: '%s' Files: '%s'\n" % (isolatename, isolatename2, isolate))
  namefile.write("%s\t%s\n" % (isolatename,isolatename2))
  assimpleroutputfile = isolatename + ".assimpler.out"
  assimplerconfile = isolatename + ".assimpler.con"
  #
  # Save sim files? (simple column format)
  #
  assimplersimfile = isolatename + ".assimpler.sim"
  if options.sim == None:
    assimplersimfilecmo = ""
  else:
    assimplersimfilecmo = " -s " + assimplersimfile
  #
  # z score for calling bases
  #
  if options.minzscore == None:
    #
    # Use default (3.29 at the time of writing)
    #
    assimplerzscore = ""
  else:
    assimplerzscore = " -z " + options.minzscore
  #
  # K-mer lengthe for use with assimpler
  #
  if options.kmerlength == None:
    #
    # Use default (17 at the time of writing)
    #
    kmerlength = ""
  else:
    kmerlength = " -k " + options.kmerlength
  #
  # Minimal score for alignment in assimpler assimpler
  #
  if options.minscore == None:
    #
    # Use default (50 at the time of writing)
    #
    minscore = ""
  else:
    minscore = " -l " + options.minscore
  #
  # Make base calls for diploid genomes
  #
  if options.diploid == None:
    #
    # Calculate nucleotide differences based on all bases called in pairs to be compared
    #
    diploid = ""
  else:
    #
    # Calculate nucleotide differences based on only bases called in all sequences
    #
    diploid = " -d "
  #
  # Run assimpler
  #
  if isolate[-1] == "z" or isolate[-2] == "z":
    cmd = "gunzip -c " + isolate + " | " + binpath + "/assimpler.py -t " + besthitfile + diploid + assimplerzscore + kmerlength + minscore + " -c " + assimplerconfile + assimplersimfilecmo + " -n " + isolatename + " > " + assimpleroutputfile
  elif isolate[-1] == "a" or isolate[-2] == "a":
    cmd = "cp " + isolatefilenames[0] + " " + isolatename
    os.system(cmd)
    # /panvol1/simon/bin/pipeline//mummer2aln.py --i ../NC_003197.fasta --ref ../NC_010468.fasta --o aligned.fa
    #cmd = binpath + "mummer2aln.py --i  " + isolatename + " --ref " + besthitfile + " --o " + assimplerconfile  + " > " + assimpleroutputfile
    cmd = "cat "  + isolate + " | " + binpath + "/simreads.py " + " | " + binpath + "/assimpler.py -t " + besthitfile + diploid + assimplerzscore + kmerlength + minscore + " -c " + assimplerconfile + assimplersimfilecmo + " -n " + isolatename  + " > " + assimpleroutputfile
  else:
    cmd = "cat "  + isolate + " | " + binpath + "/assimpler.py -t " + besthitfile + diploid + assimplerzscore + kmerlength + minscore + " -c " + assimplerconfile + assimplersimfilecmo + " -n " + isolatename  + " > " + assimpleroutputfile
  #outputfile.write("# %s\n" % (cmd))
  os.system(cmd)
  cmd = "cat " + assimplerconfile + " >> map.fsa"
  os.system(cmd)
  i += 1
namefile.close()
#
# Calculate difference matrix
#

#
# z score for calling bases
#
if options.allcalled == None:
  #
  # Calculate nucleotide differences based on all bases called in pairs to be compared
  #
  allcalled = ""
else:
  #
  # Calculate nucleotide differences based on only bases called in all sequences
  #
  allcalled = " -a "
#
#
#
cmd = binpath + "/nctree.py " + allcalled + "-i map.fsa -o map.mat > nctree.log"
#outputfile.write("# %s\n" % (cmd))
os.system(cmd)
#
# Make tree
#
os.system("touch infile")
os.system("touch outfile")
os.system("touch outtree")
os.system("mv infile infile.old")
os.system("mv outfile outfile.old")
os.system("mv outtree outtree.old")
os.system("cp map.mat infile")

#outputfile.write("# Neighbor infile\n")
#tmpfile = open("infile","r")
#for line in tmpfile:
#  outputfile.write("%s" % (line))
#tmpfile.close()


cmd = "echo \"N\nY\n\" | " + binpath + "neighbor  >  neighbor.log"
cmdtxt = "echo \"N\\nY\\n\" | " + binpath + "neighbor  >  neighbor.log"
#outputfile.write("# %s\n" % (cmdtxt))
os.system(cmd)
#
# Print outfile
#
#outputfile.write("# Neighbor outfile\n")
#tmpfile = open("outfile","r")
#for line in tmpfile:
#  outputfile.write("%s" % (line))
#tmpfile.close()
#
# Print outtree
#
#outputfile.write("# Neighbor outtree\n")
#tmpfile = open("outtree","r")
#for line in tmpfile:
#  outputfile.write("%s" % (line))
#tmpfile.close()
#
# Change name in infile and print
#
cmd = binpath + "/chnam.py -t2 -s names -i infile > infile.names"
#outputfile.write("# %s\n" % (cmd))
os.system(cmd)
#
#
#
#outputfile.write("# Neighbor infile with names from filenames\n")
#tmpfile = open("infile.names","r")
#for line in tmpfile:
#  outputfile.write("%s" % (line))
#tmpfile.close()
#
# Change name in outfile and print
#
cmd = binpath + "/chnam.py -t2 -s names -i outfile > outfile.names"
outputfile.write("# %s\n" % (cmd))
os.system(cmd)
#
#
#
#outputfile.write("# Neighbor outfile with names from filenames\n")
#tmpfile = open("outfile.names","r")
#for line in tmpfile:
#  outputfile.write("%s" % (line))
#tmpfile.close()
#
# Change names in tree and print
#
cmd = binpath + "/chnam.py -t2 -s names -i outtree > tree.newick"
#outputfile.write("# %s\n" % (cmd))
os.system(cmd)
#
#
#
#outputfile.write("# Neighbor outtree with names from filenames\n")
#tmpfile = open("tree.newick","r")
#for line in tmpfile:
#  outputfile.write("%s" % (line))
#tmpfile.close()
#
# Call Martins (or other) code to make visualization of tree
#


#
# Done
#

#
# Close files
#
#t1 = time.time()
#sys.stdout.write("%s %d %s\n" % ("# Finishing. Time used: ", int(t1-t0)," seconds"))
#inputfile.close()
#templatefile.close()
#outputfile.close()
outputfile.write("# </pre>\n")
sys.stderr.write("Done\n")
