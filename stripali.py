#!/usr/bin/env python

# Copyright (c) 2014, Ole Lund, Technical University of Denmark
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Import libraries
#
import sys, time
import os
import gc
import numpy as np
import array
from optparse import OptionParser
from operator import itemgetter
import re
#import gzip
#
# Functions
#
# Start time to keep track of progress
#
t0 = time.time()
#
#
#
etta = 0.001
#
# Parse command line options
#
parser = OptionParser()
parser.add_option("-i", "--inputfile", dest="inputfilename", help="read from INFILE", metavar="INFILE")
parser.add_option("-o", "--outputfile", dest="outputfilename", help="write to OUTFILE", metavar="OUTFILE")
#parser.add_option("-d", "--difffile", dest="difffilename", help="write to DIFFFILE", metavar="DIFFFILE")
#parser.add_option("-a", "--allcalled", dest="allcalled", action="store_true", help="Only use positions called in all strains")
(options, args) = parser.parse_args()
#
# Open files
#
#
# File with reads
#
if options.inputfilename != None:
  inputfile = open(options.inputfilename,"r")
else:
  inputfile = sys.stdin
#
# File for general output
#
if options.outputfilename != None:
  outputfile = open(options.outputfilename,"w")
else:
  outputfile = sys.stdout
#
# File for differences between samples
#
#if options.difffilename != None:
#  difffile = open(options.difffilename,"w")
#
# Read Input fasta file
#
inputseq = []
inputseqsegments = []
consensusseq = []
inputname = []
inputdesc = []
Ninputs=0
i=0
#if options.inputfilename != None:
if 1 != 0:
  # allways true
  t1 = time.time()
  #sys.stdout.write("%s %d %s\n" % ("# Time used: ", int(t1-t0)," seconds"))
  sys.stdout.write("%s\n" % ("# Reading inputfile"))
  for line in inputfile:
    fields=line.split()
    if len(line)>1:
      if fields[0][0] == ">":
        #print "# input ",line
	if (i>0):
	  inputseq[-1] = ''.join(inputseqsegments)
	  #print len(inputseq),len(inputseq[-1]),len(inputseq[len(inputseq)-1])
        del inputseqsegments
        inputseqsegments = []
	i=0
        inputseq.append("")
        consensusseq.append("")
        inputname.append(fields[0][1:])
        inputdesc.append(re.sub(r"^[^\s]+\s","",line.strip()))
      else:
        #inputseq[-1] = inputseq[-1]  + fields[0]
	#print i
	#print fields[0]
        inputseqsegments.append("")
        inputseqsegments[i] = fields[0]
	i+=1
  inputseq[-1] = ''.join(inputseqsegments)
  #print len(inputseq),len(inputseq[-1]),len(inputseq[len(inputseq)-1])
del inputseqsegments

#
# Find number of strains and chromosomes
#
nseq = len(inputseq)
lseq0 = len(inputseq[0])
nchrom=1
lseqmax = lseq0
while (len(inputseq[nchrom]) != lseq0):
  sys.stdout.write("# Length of chromosome %d: %d\n" % (nchrom,len(inputseq[nchrom-1])))
  if (len(inputseq[nchrom]) > lseqmax):
    lseqmax = len(inputseq[nchrom])
  nchrom +=1
sys.stdout.write("# Length of chromosome %d: %d\n" % (nchrom,len(inputseq[nchrom-1])))
nstrain = int(nseq/nchrom)
sys.stdout.write("# Number of strains: %d\n" % (nstrain))
sys.stdout.write("# Number of chromosomes: %d\n" % (nchrom))
#for h in xrange(0, nchrom):


Nallcalled=0
Nnonconserved=0
Npos=0
Nalipos=0

alignment = []

for l in xrange(0, nstrain):
  alignment.append("")

for n in xrange(0, nchrom):
  sys.stdout.write("# Chromosome: %d\n" % (n+1))
  for j in xrange (0,len(inputseq[n])):
    #print "pos ",j
    allcalled = 1
    conserved = 1
    Npos += 1
    for l in xrange(0, nstrain):
      #print "strain ",l
      i = n+l*nchrom
      if (inputseq[i][j] == "N"):
        #
	# At least one strain contains an N
	#
        allcalled = 0
      if (inputseq[i][j] != inputseq[n][j]):
        #
        # Not all bases are the same (as the first one - which means they are not all the same)
	#
        conserved=0
    if (allcalled == 1):
      Nallcalled +=1
      if (conserved == 0):
        Nnonconserved +=1
        for l in xrange(0, nstrain):
          #print "strain ",l
          i = n+l*nchrom
	  alignment[l] = alignment[l] + inputseq[i][j]
          #outputfile.write("%s" % (inputseq[i][j]))
        #outputfile.write("\n")
	Nalipos += 1
sys.stdout.write("# Number of positions called in all strains: %d\n" % (Nallcalled))
sys.stdout.write("# Number of non concerved positions called in all strains %d\n" % (Nnonconserved))
#
# Print stripped alignment
#
if len(alignment[0]) > 0:
  for l in xrange(0, nstrain):
    i = l*nchrom
    #print alignment[l]
    start=0
    outputfile.write(">%s %s\n" % (inputname[i], inputdesc[i]))
    start=0
    while start < len(alignment[l]):
      outputfile.write("%s\n" % (alignment[l][start:start+60]))
      start +=60


#
# Close files
#
t1 = time.time()
sys.stdout.write("%s %d %s\n" % ("# Finishing. Time used: ", int(t1-t0)," seconds"))
#inputfile.close()
#templatefile.close()
#outputfile.close()
