#!/usr/bin/env python

# Copyright (c) 2014, Ole Lund, Technical University of Denmark
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Import libraries
#
import sys, time
import os
import gc
from optparse import OptionParser
from operator import itemgetter
import re
from math import sqrt
#import gzip
#
# Functions
#
# Construct the reverse complemant from a sequence
#
def reversecomplement(seq):
    '''Reverse complement'''
    comp = ''
    for s in seq:
        if s == 'A': comp = comp + 'T'
        elif s == 'T': comp = comp + 'A'
        elif s == 'C': comp = comp + 'G'
        elif s == 'G': comp = comp + 'C'
        else: comp = comp + s
    return comp[::-1]
#
# Color to base
#
color2base = {
  "A0" : 'A',
  "A1" : 'C',
  "A2" : 'G',
  "A3" : 'T',
  "T0" : 'T',
  "T1" : 'G',
  "T2" : 'C',
  "T3" : 'A',
  "C0" : 'C',
  "C1" : 'A',
  "C2" : 'T',
  "C3" : 'G',
  "G0" : 'G',
  "G1" : 'T',
  "G2" : 'A',
  "G3" : 'C'
}

tooneletter = {
  #
  # A  adenosine          C  cytidine             G  guanine
  # T  thymidine          N  A/G/C/T (any)        U  uridine
  # K  G/T (keto)         S  G/C (strong)         Y  T/C (pyrimidine)
  # M  A/C (amino)        W  A/T (weak)           R  G/A (purine)
  # B  G/T/C              D  G/A/T                H  A/C/T
  # V  G/C/A              -  gap of indeterminate length
  #   A C G T
  # A A M R W
  # C M C S Y
  # G R S G K
  # T W Y K T
  #
  "AA" : 'A',
  "AC" : 'M',
  "AG" : 'R',
  "AT" : 'W',
  "AN" : 'A',
  "TA" : 'W',
  "TC" : 'Y',
  "TG" : 'K',
  "TT" : 'T',
  "TN" : 'T',
  "CA" : 'M',
  "CC" : 'C',
  "CG" : 'S',
  "CT" : 'Y',
  "CN" : 'C',
  "GA" : 'R',
  "GC" : 'S',
  "GG" : 'G',
  "GT" : 'K',
  "GN" : 'G',
  "NA" : 'A',
  "NC" : 'C',
  "NG" : 'G',
  "NT" : 'T',
  "NN" : 'N'
}


#
# Start time to keep track of progress
#
t0 = time.time()
#
#
#
etta = 0.001
#
# Parse command line options
#
parser = OptionParser()
parser.add_option("-i", "--inputfile", dest="inputfilename", help="read from INFILE", metavar="INFILE")
parser.add_option("-t", "--templatefile", dest="templatefilename", help="read from TEMFILE", metavar="TEMFILE")
parser.add_option("-o", "--outputfile", dest="outputfilename", help="write to OUTFILE", metavar="OUTFILE")
parser.add_option("-s", "--columnfile", dest="columnfilename", help="write to column file", metavar="COLUMN")
parser.add_option("-c", "--consensusfile", dest="consensusfilename", help="write fasta assembly to CONFILE", metavar="CONFILE")
parser.add_option("-n", "--entryname", dest="entryname", help="Set name in fastafile to ENTRYNAME", metavar="ENTRYNAME")
parser.add_option("-u", "--unusedfile", dest="unusedfilename", help="write to UNUSEDFILE", metavar="UNUSEDFILE")
parser.add_option("-m", "--mincoverage", dest="mincoverage", help="lower bound of coverage for assembly to print out. Dekault is 0 (~1 read )", metavar="MINCOVERAGE")
parser.add_option("-a", "--maxlatcalls", dest="maxaltcalls", help="maximum fraction of alternative calls. Default is 0.1", metavar="MAXALTCALLS")
parser.add_option("-z", "--minzscore", dest="minzscore", help="Minimum Z-score to call bases. Deafault is 3.29 ~ p = 0.001", metavar="MINZSCORE")
parser.add_option("-M", "--mismatchscore", dest="mismatchscore", help="Mismatch score (penalty). Deafault is 3", metavar="KMERLENGTH")
parser.add_option("-k", "--kmerlength", dest="kmerlength", help="k-mer length to use in hash table. default is 17", metavar="MISMATCHSCORE")
parser.add_option("-l", "--minscore", dest="minscore", help="Minscore to accept mapping. Deafault is 50", metavar="MINSCORE")
parser.add_option("-f", "--frogdna", dest="usefrogdna", action="store_true", help="Use template DNA in regions with no reads")
parser.add_option("-e", "--extendtemplate", dest="extendtemplate", action="store_true", help="extend templates if reads overhang in the end/create new templat if no match is found")
parser.add_option("-d", "--diploid", dest="diploid", action="store_true", help="Make base calls for diploid genomes")
(options, args) = parser.parse_args()
#
# Open files
#
#
# File with reads
#
if options.inputfilename != None:
  inputfile = open(options.inputfilename,"r")
else:
  inputfile = sys.stdin
#
# File with templare
#
if options.templatefilename != None:
#  fileName, fileExtension = os.path.splitext(options.templatefilename)
#  if (fileExtension == ".gz"):
#    templatefile = gzip.open(options.templatefilename,"r")
#  else:
  templatefile = open(options.templatefilename,"r")
#else:
#  sys.exit("No template file specified")
#
# File for general output
#
if options.outputfilename != None:
  outputfile = open(options.outputfilename,"w")
else:
  outputfile = sys.stdout
#
# File for consensus sequence in fasta format
#
if options.consensusfilename != None:
  consensusfile = open(options.consensusfilename,"w")
else:
  consensusfile = sys.stdout
#
# Entryname in fastafile
#
if options.entryname != None:
  entryname = options.entryname + " "
else:
  entryname = ""
#
# File for assembly in columns in SAM like format (which is not really sam like)
#
if options.columnfilename != None:
  columnfile = open(options.columnfilename,"w")
#
# File for unused reads
#
if options.mincoverage != None:
  mincoverage = options.mincoverage
else:
  mincoverage = 0
#
#
#
if options.maxaltcalls != None:
  maxaltcalls = float(options.maxaltcalls)
else:
  maxaltcalls = 0.1
mincovfrac = float(1.0-maxaltcalls)
#
#
#
if options.minzscore != None:
  minzscore = float(options.minzscore)
else:
  minzscore = 3.29

# Penalty for mismatch
if options.mismatchscore != None:
  mismatchscore = -1.0*float(options.mismatchscore)
else:
  mismatchscore = -3

# Minscore to accept mapping
if options.minscore != None:
  minscore = 1.0*float(options.minscore)
else:
  minscore = 50

# K-mer length to use for mapping
if options.kmerlength != None:
  kmerlength = int(options.kmerlength)
else:
  kmerlength = 17

#print mincovfrac, maxaltcalls
#
# Minimum coverage to print out
#
if options.unusedfilename != None:
  unusedfile = open(options.unusedfilename,"w")
#
# Read Template fasta file
#
templateseq = []
templateseqsegments = []
consensusseq = []
templatename = []
templatedesc = []
Ntemplates=0
i=0
if options.templatefilename != None:
  sys.stdout.write("%s\n" % ("# Reading templatefile"))
  for line in templatefile:
    fields=line.split()
    if len(line)>1:
      if fields[0][0] == ">":
        #print "# Template ",line
	if (i>0):
	  templateseq[-1] = ''.join(templateseqsegments)
        del templateseqsegments
        templateseqsegments = []
	i=0
        templateseq.append("")
        consensusseq.append("")
        templatename.append(fields[0][1:])
        templatedesc.append(re.sub(r"^[^\s]+\s","",line.strip()))
      else:
        #templateseq[-1] = templateseq[-1]  + fields[0]
	#print i
	#print fields[0]
        templateseqsegments.append("")
        templateseqsegments[i] = fields[0]
	i+=1
  templateseq[-1] = ''.join(templateseqsegments)
del templateseqsegments
#
# Make nmer index for templatefiles
#
sys.stdout.write("%s\n" % ("# Making index"))
templateindex = {}
# Length of K-mer
oligolen = kmerlength
# Steps between k-mers in template (large value reduces memory usage)
kmerstep = kmerlength
#print "# len",len(templateseq)
#print "# len2",len(templateseq[0])
for i in xrange(0, len(templateseq)):
  seqlen = len(templateseq[i])
  #print "# seqNo ", i, "#seqlen", seqlen
  for j in xrange(0, seqlen-oligolen+1,kmerstep):
    #take larger steps to conserve memory
    submer = templateseq[i][j:j+oligolen]
    name = ("%d%s%d" % (i,"_",j))
    if submer in templateindex:
      #templateindex[submer] = templateindex[submer]+","+name
      # below is just to test
      #templateindex[submer] = name
      templateindex[submer].append(name)
      #print "# Old submer ",submer, templateindex[submer]
    else:
       templateindex[submer] = [name,]
       #print "# New submer ",submer, templateindex[submer]
#
# Initialize read depth matrices
#
sys.stdout.write("%s\n" % ("# Initializing read statistics"))
Nmatch = []
NmatchA = []
NmatchT = []
NmatchG = []
NmatchC = []
# numpy version
#for i in xrange(0, len(templateseq)):
#  Nmatch.append(np.zeros((len(templateseq[i])),dtype=np.uint8))
#  NmatchA.append(np.zeros((len(templateseq[i])),dtype=np.uint8))
#  NmatchT.append(np.zeros((len(templateseq[i])),dtype=np.uint8))
#  NmatchG.append(np.zeros((len(templateseq[i])),dtype=np.uint8))
#  NmatchC.append(np.zeros((len(templateseq[i])),dtype=np.uint8))

# non-numpy version
for i in xrange(0, len(templateseq)):
#  Nmatch.append([0,0,0,0,0]*len(templateseq[i]))
  Nmatch.append([0]*len(templateseq[i]))
  NmatchA.append([0]*len(templateseq[i]))
  NmatchT.append([0]*len(templateseq[i]))
  NmatchG.append([0]*len(templateseq[i]))
  NmatchC.append([0]*len(templateseq[i]))
#
# Scan input file for matches to template
#
# Number of reads read
nreads =0
# Number of lookups of oligos in template databases
oligotests=0
# Number of oligos matching template
oligohits=0
# Number og hits that are significant after extension
fullhits=0
# Score for matching nucleotides
matchscore = 1
# Score for alitning to nucleotides with low read depth
singlescore = 0.1
# Number of unmatched reads
Nunmatchedreads= 0
# Minimum score for counting match as significant
#minscore = 50
# Min mength of read for it to be mapped
minreadlength = int(minscore)
# Stepsize for selecting next oligo for searching for match in template
oligosteps = 1
sys.stdout.write("%s\n" % ("# Scanning input file for matches to template"))
t1 = time.time()
for line in inputfile:
  # Loop over all reads
  try:
    fields=line.split()
    if fields[0][0] == ">":
      # Fasta file
      name = fields[0][1:]
      line = inputfile.next()
      fields=line.split()
      try:
        seq = fields[0]
      except:
        seq = ""
    elif fields[0][0] == "@":
      # Fastq file
      line = inputfile.next()
      fields=line.split()
      try:
        seq = fields[0]
        if fields[0][1] in ['0','1','2','3']:
	  seq2=seq
	  seq = seq2[0]
	  for i in xrange (0,len(seq2)-1):
	    seq = seq + color2base[seq[i]+seq2[i+1]]
      except:
        seq = ""
      line = inputfile.next()
      line = inputfile.next()
    else:
      # File with only sequences
      fields=line.split()
      try:
        seq = fields[0]
      except:
        seq = ""
  except:
    break

  #
  nreads += 1
  if nreads % 1000 == 0:
    t2 = time.time()
    sys.stdout.write("\r# %s nreads (%s nreads / s)" % ("{:,}".format(nreads), "{:,}".format(int(nreads / (t2-t1)))))
    sys.stdout.flush()
  #
  # Search for sequence in template
  #
  foundmatch = 0
  if len(seq) >=minreadlength:
    # Filter out short reads
    for qseq in [seq,reversecomplement(seq)]:
      #print "Testing qseq sequence: ",qseq
      # Try both the sequence and its reverse complement
      #foundmatch = False
      qseqlen = len(qseq)
      #for i in xrange(0, 1):
      for i in xrange(0, qseqlen-oligolen,oligosteps):
        # Look for first exact match of a subsequence of length oligolen
	# Taking longer steps here may speed things up (and degrade perormance a bit!)
	if (foundmatch > 0):
	  break
	  # Go to reverse complement or next sequence if mactch is found
	#submerstart = int(round((qseqlen-oligolen)/2))
	submerstart = i
	submer = qseq[submerstart:submerstart+oligolen]
	#print "Seeking submer", submer, oligolen, submerstart, qseqlen, qseq
	oligotests += 1
        if submer in templateindex:
	  #  Subsequence is found in template
	  foundmatch += 1
	  oligohits += 1
	  #print "Seeking submer", submer, oligolen, submerstart, qseqlen, qseq
	  #print "Found submer", submer, templateindex[submer]
	  # Make vector of matches
	  matchindexes = templateindex[submer]
	  #print "# Matchindexes",matchindexes, "submer: ", submer
	  #matchindexes2= sorted(matchindexes.items(), key = itemgetter(1), reverse=False)
	  for matchindex in matchindexes:
	    # split each match into sequence No (j) and nucleotide number in sequence (k)
	    #print "# Matchindexes",matchindexes, "# Matchindex",matchindex, "Submer: ",submer
	    #sys.exit()
            (jj,kk) = matchindex.split("_")
	    j = int(jj)
	    k = int(kk)
	    #
	    # Extend match to the left and right to find max scoring segment
	    #
	    #extend left
	    l = submerstart-1
	    m = k-1
	    leftscore = 0
	    bestleftscore = 0
	    bestleftqstart = submerstart
	    bestlefttstart = k
	    while (l >= 0 and m >= 0 and m > k-qseqlen):
	      # Search within sequeces and not longer than length of qseqlen away
	      # The last is really only needed if code is generalized to gaped alignments
	      # The following 10 lines is maybe where speed may be gained
	      if (qseq[l:l+1] == templateseq[j][m:m+1]):
		leftscore += matchscore
	      else:
	        leftscore += mismatchscore
	      if (leftscore > bestleftscore):
		bestleftscore = leftscore
		bestleftqstart = l
		bestlefttstart = m
              l -= 1
	      m -= 1
	    #extend right - see comments above for extend left
	    l = submerstart+oligolen
	    m = k+oligolen
	    rightscore = 0
	    bestrightscore = rightscore
	    bestrightqend = l
	    bestrighttend = m
	    templateseqlen = len(templateseq[j])
	    while (l< qseqlen  and m < templateseqlen and m <= k+oligolen+qseqlen):
	      #print "# While ", l, qseqlen,m,len(templateseq[j]),m,k+oligolen+qseqlen
	      disttoend = (templateseqlen -1) - bestrighttend
	      if (qseq[l:l+1] == templateseq[j][m:m+1]):
	        rightscore += matchscore
	      # Allow well scoring alignments to be extended if it can extend long to the right of template
	      elif (disttoend < 5 and totalscore >= minscore and qseqlen-bestrightqend-1 > 5):
	        rightscore += singlescore
	      else:
                rightscore += mismatchscore
	      if (rightscore > bestrightscore):
	        bestrightscore = rightscore
	        bestrightqend = l
	        bestrighttend = m
	      l += 1
	      m += 1
	    totalscore = bestleftscore+bestrightscore+oligolen*matchscore
	    #print "# Totalscore ",totalscore
	    #print "# bestleftqstart ",bestleftqstart
	    #print "# bestrightqend+1 ",bestrightqend+1
	    #print "# bestlefttstart ",bestlefttstart
	    #print "# bestrighttend+1 ",bestrighttend+1
	    #print "#Q ",qseq[bestleftqstart:bestrightqend+1]
	    #print "#T ",templateseq[j][bestlefttstart:bestrighttend+1]
	    if (totalscore >= minscore):
              fullhits += 1
	      #print "# j ",j
	      #print "# Totalscore ",totalscore
	      #print "# bestleftqstart ",bestleftqstart
	      #print "# bestrightqend+1 ",bestrightqend+1
	      #print "# bestlefttstart ",bestlefttstart
	      #print "# bestrighttend+1 ",bestrighttend+1
	      #print "#Q ",qseq[bestleftqstart:bestrightqend+1]
	      #print "#T ",templateseq[j][bestlefttstart:bestrighttend+1]
	      for n in xrange (0,bestrighttend-bestlefttstart):
	        #print "# n ",n
	        #print "# templateseqlen ",templateseqlen
	        Nmatch[j][n+bestlefttstart] += 1
	        nucleotide =  qseq[n+bestleftqstart:n+bestleftqstart+1]
		if (nucleotide == 'A'):
		  NmatchA[j][n+bestlefttstart] += 1
		elif (nucleotide == 'T'):
		  NmatchT[j][n+bestlefttstart] += 1
		elif (nucleotide == 'G'):
		  NmatchG[j][n+bestlefttstart] += 1
		elif (nucleotide == 'C'):
		  NmatchC[j][n+bestlefttstart] += 1
              if options.extendtemplate != None:
	        # Extend reads to the right
		disttoend = (templateseqlen -1) - bestrighttend
		if disttoend == 0: # or <2 eg
	        #if bestrighttend == templateseqlen -1:
		  # Alignment goes all the way to the end of the template
		  for o in xrange (disttoend,qseqlen-bestrightqend-1):
		  #for o in xrange (0,qseqlen-bestrightqend-1):
		    tpos = templateseqlen+o
		    qpos = bestrightqend+1+o
		    templateseq[j] = templateseq[j] + qseq[qpos:qpos+1]
	            #Nmatch[j][tpos] = 0
	            #NmatchA[j][tpos] = 0
	            #NmatchT[j][tpos] = 0
	            #NmatchG[j][tpos] = 0
	            #NmatchC[j][tpos] = 0
	            Nmatch[j].append(0)
	            NmatchA[j].append(0)
	            NmatchT[j].append(0)
	            NmatchG[j].append(0)
	            NmatchC[j].append(0)
		    # Update read depth statistics - i am not sure the below section works
		    nucleotide = qseq[qpos:qpos+1]
		    if (nucleotide == 'A'):
		      NmatchA[j][tpos] += 1
		    elif (nucleotide == 'T'):
		      NmatchT[j][tpos] += 1
		    elif (nucleotide == 'G'):
		      NmatchG[j][tpos] += 1
		    elif (nucleotide == 'C'):
		      NmatchC[j][tpos] += 1
		    # add kmer to index sequence (I should acrually go in kmer steps here)
		    indexseq = qseq[qpos-oligolen+1:qpos+1]
		    name = ("%d%s%d" % (j,"_",tpos-oligolen+1))
                    if indexseq in templateindex:
                      templateindex[indexseq].append(name)
                      #print "#Extending  Old indexseq ",indexseq, templateindex[indexseq]
                    else:
                      templateindex[indexseq] = [name,]
                      #print "#Extending New indexseq ",indexseq, templateindex[indexseq]
  if (foundmatch == 0):
    Nunmatchedreads += 1;
    if options.unusedfilename != None:
      unusedfile.write("%s\n" % (seq))
    #
    # Add new template
    #
    #print "# Adding template:", seq
    if options.extendtemplate != None:
      i = len(templateseq)
      templateseq.append("")
      consensusseq.append("")
      templatename.append("New template")
      templatedesc.append("")
      templateseq[i] = seq
      seqlen = len(seq)
      #print "# seqNo ", i, "#seqlen", seqlen
      for j in xrange(0, seqlen-oligolen+1,kmerstep):
        #take larger steps to conserve memory
        submer = templateseq[i][j:j+oligolen]
        name = ("%d%s%d" % (i,"_",j))
        if submer in templateindex:
          templateindex[submer].append(name)
          #print "#New template Old submer ",submer, templateindex[submer]
        else:
          templateindex[submer] = [name,]
          #print "#New template New submer ",submer, templateindex[submer]
      Nmatch.append([0]*len(templateseq[i]))
      NmatchA.append([0]*len(templateseq[i]))
      NmatchT.append([0]*len(templateseq[i]))
      NmatchG.append([0]*len(templateseq[i]))
      NmatchC.append([0]*len(templateseq[i]))
#
# Print template based assembly
#
sys.stdout.write("\n")
sys.stdout.write("%s\n" % ("# Writing template based assembly"))
# Minimum coverage for calling a base
#mincoverage = 1000
# total number of matches summed over all positions
Nmatchsum = 0
Nmatchsumv = []
# Number af positions covered by atleast one read
covered = 0
# Number af positions covered by atleast mincoverage reads
mincovered = 0
# Length of template
templatelentot = 0
#
mincoveredcon = []
#mincoveredcon.append("0"*len(templateseq))
for i in xrange(0, len(templateseq)):
  mincoveredcon.append("0")
#print len(templateseq)
#print mincoveredcon[0]
#print mincoveredcon[1]
#
# If user have selected sam style outputfile then write header
#
if options.columnfilename != None:
  columnfile.write("%s\n" % ("# index templatename templateseq consensusseq No_of_matches  No_of_A No_of_T No_of_G No_of_C"))
#
# Call bases
#
#
for i in xrange(0, len(templateseq)):
  #
  # Loop over template sequences
  #
  Nmatchsumv.append(0)
  consensuslist = []
  templatelentot += len(templateseq[i])
  #print i
  mincoveredcon[i] =0
  for j in xrange (0,len(templateseq[i])):
    #
    # Loop over positions in templatefile
    #
    if options.usefrogdna != None:
      # Use template (frog in Jurassic Parc) DNA
      consensus = templateseq[i][j]
    else:
      # The more conservative choice: if not enough reads call it as an N
      consensus = 'N'
      nextbase = 'N'
    #print consensus, Nmatch[i][j], NmatchA[i][j], NmatchT[i][j], NmatchC[i][j], NmatchG[i][j], float(NmatchA[i][j])/(float(Nmatch[i][j])+etta), mincovfrac
    maxcoverage = mincoverage
    if options.diploid == None:
      #
      # Not diploid genome
      #
      if (float(NmatchA[i][j])>maxcoverage):
        maxcoverage = NmatchA[i][j]
	nextbase = 'A'
      if (float(NmatchT[i][j])>maxcoverage):
        maxcoverage = NmatchT[i][j]
	nextbase = 'T'
      if (float(NmatchG[i][j])>maxcoverage):
        maxcoverage = NmatchG[i][j]
	nextbase = 'G'
      if (float(NmatchC[i][j])>maxcoverage):
        maxcoverage = NmatchC[i][j]
	nextbase = 'C'
      tot = float(Nmatch[i][j])
      alt =  tot - maxcoverage
      zscore = (maxcoverage-alt)/sqrt(tot+etta)
      if (maxcoverage > mincoverage and maxcoverage/(tot+etta)>mincovfrac and zscore > minzscore):
        consensus = nextbase
    else:
      #
      # Diploid genome base calls
      #
      firstbase = 'N'
      secondbase = 'N'
      nfirstbase = 0
      nsecondbase = 0
      # Find most common base
      nfirstbase = mincoverage
      if (float(NmatchA[i][j])>nfirstbase):
        nfirstbase = NmatchA[i][j]
	firstbase = 'A'
      if (float(NmatchT[i][j])>nfirstbase):
        nfirstbase = NmatchT[i][j]
	firstbase = 'T'
      if (float(NmatchG[i][j])>nfirstbase):
        nfirstbase = NmatchG[i][j]
	firstbase = 'G'
      if (float(NmatchC[i][j])>nfirstbase):
        nfirstbase = NmatchC[i][j]
	firstbase = 'C'
      # Find second most common base
      nsecondbase = mincoverage
      if (float(NmatchA[i][j])>nsecondbase and firstbase != 'A'):
        nsecondbase = NmatchA[i][j]
	secondbase = 'A'
      if (float(NmatchT[i][j])>nsecondbase and firstbase != 'T'):
        nsecondbase = NmatchT[i][j]
	secondbase = 'T'
      if (float(NmatchG[i][j])>nsecondbase and firstbase != 'G'):
        nsecondbase = NmatchG[i][j]
	secondbase = 'G'
      if (float(NmatchC[i][j])>nsecondbase and firstbase != 'C'):
        nsecondbase = NmatchC[i][j]
	secondbase = 'C'
      #
      # Find significance of base calls
      #
      tot = float(Nmatch[i][j])
      alt =  tot - (nfirstbase+nsecondbase)
      zscore = (nfirstbase-alt)/sqrt(tot-nsecondbase+etta)
      if (nfirstbase > mincoverage and nfirstbase/(tot-nsecondbase+etta)>mincovfrac and zscore > minzscore):
        # Call for most comon base is significant
	zscore = (nsecondbase-alt)/sqrt(tot-nfirstbase+etta)
	if (nsecondbase > mincoverage and nsecondbase/(tot-nfirstbase+etta)>mincovfrac and zscore > minzscore):
          # heterozygote call Call for secondmost comon base is significant
	  diconsensus = firstbase + secondbase
	else:
	  # homozygore call
	  diconsensus = firstbase + firstbase
      else:
        diconsensus = 'NN'
      consensus = tooneletter[diconsensus]
    consensuslist.append(consensus)
    #
    # Write columns in column format
    #
    if options.columnfilename != None:
      columnfile.write("%10d %20s %5d %1s %1s %5d %5d %5d %5d %5d\n" % (
        i+1,
	templatename[i],
	j+1,
	templateseq[i][j],
	consensus, Nmatch[i][j],
	NmatchA[i][j],
	NmatchT[i][j],
	NmatchG[i][j],
	NmatchC[i][j]))
    # Update asembly statistics
    if (Nmatch[i][j]>0):
      Nmatchsumv[i] +=  Nmatch[i][j]
      covered += 1
    if (Nmatch[i][j]>=mincoverage):
      mincovered += 1
      mincoveredcon[i] += 1
  # The following is the fast way to construct sequences according to an internet site, and it really speed things up
  consensusseq[i] = ''.join(consensuslist)
  del consensuslist
  Nmatchsum += Nmatchsumv[i]
  sys.stdout.write("%s %d %s %f %s %s\n" %
    ("# Average cov template no.",i+1, ":", float(Nmatchsumv[i])/(float(len(templateseq[i]))+etta),templatename[i],templatedesc[i]))
  #
  # Write consensusseq as fastafile
  #
  if options.consensusfilename != None:
    if mincoveredcon[i] > 0:
      start=0
      consensusfile.write(">%sContig_%d %s %s\n" % (entryname,i+1, templatename[i], templatedesc[i]))
      start=0
      while start < len(templateseq[i]):
        consensusfile.write("%s\n" % (consensusseq[i][start:start+60]))
        start +=60
#
# Print statistics
#
sys.stdout.write("%s %d\n" % ("# Oligotests:   ", oligotests))
sys.stdout.write("%s %d\n" % ("# Fullhits:     ", fullhits))
sys.stdout.write("%s %d\n" % ("# Nmatchsum:    ", Nmatchsum))
sys.stdout.write("%s %f\n" % ("# Average cov: ", float(Nmatchsum)/float(templatelentot+etta)))
sys.stdout.write("%s %d\n" % ("# templatelentot:  ", templatelentot))
sys.stdout.write("%s %d\n" % ("# covered:      ", covered))
sys.stdout.write("%s %f\n" % ("# frac covered: ", float(covered)/float(templatelentot+etta)))
sys.stdout.write("%s %d\n" % ("# mincovered:   ", mincovered))
sys.stdout.write("%s %f\n" % ("# frac min covered: ", float(mincovered)/float(templatelentot+etta)))
sys.stdout.write("%s %d\n" % ("# N unmap read: ", Nunmatchedreads))
#
#
#
#print "Memory used5",sum([sys.getsizeof(o) for o in gc.get_objects()])
#print "Memory used5b",resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
#
# Close files
#
t2 = time.time()
sys.stdout.write("%s %d %s\n" % ("# Finishing. Time used: ", int(t2-t0)," seconds"))
#inputfile.close()
#templatefile.close()
#outputfile.close()
